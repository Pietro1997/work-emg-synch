from config import BASEPATH

#%%
#importo librerie necessarie per l'esecuzione del programma

import os
import pandas as pd
import pyphysio as ph
from physynch import surrogate_IAAFT, compute_distances_golland
import numpy as np

#funzione per preprocessare il segnale dopo averlo caricato
def preprocess_signal(x):
    #fai uno smoothing del segnale
    x = ph.ConvolutionalFilter('rect', CONV_WINDOW)(x)
    return(x)

#funzione di "normalizzazione" del segnale
def normalize_scale(x):
    return((x - np.min(x))/np.std(x))

#%% impostazioni del parametri principali del calcolo della sincronia

# delay massimo da usare per il calcolo della CC 
MAX_DELAY = 1 #seconds

# parametro di smoothing del segnale
CONV_WINDOW = 5

#%% imposto directories
DATADIR = f'{BASEPATH}/data/envelope'

#il nome della cartella ha al suo interno MAX_DELAY
# cioe' se MAX_DELAY = 2, il nome sara' cc_EMG_2
#questa cartella va creata manualmente
#utile provare con vari MAX_DELAY (es 1,2,5 secondi) e quindi avere piu' cartelle risultato
OUTDIR = f'{BASEPATH}/data/cc_EMG_{MAX_DELAY}' 

TRGDIR = f'{BASEPATH}/data/trg'

#nomi dei video
video_names    = ['HS',    'TIT', 'WD',   'RELAX', 'NH',   'PEN']
video_emotions = ['EMBRS', 'SAD', 'FEAR', 'RELAX', 'LOVE', 'PRIDE']


#altre impostazioni, non cambiare!
# distance parameters
DISTANCE = 'cc'
F_RESAMP = 10
LAG = MAX_DELAY*F_RESAMP

STANDARDIZE = True
NORMALIZE = True

#%% 
#=========================================================================
# Questa sezione carica tutti i dati - Richiede un po' di tempo
#===================================

#metto solo i commenti ai passaggi principali

#ottieni la lista delle diadi e per ogni diade il tipo di relazione
dyad_list = np.unique([x.split('_')[0] for x in os.listdir(DATADIR)])
relation = np.array([D[0] for D in dyad_list])

#contenitore per tutti i dati
data = {}

#processo un gruppo di relazione alla volta
for RELATION in ['U', 'F', 'L']:
    
    #seleziona solo le diadi appartenenti al gruppo in esame
    selected_dyads = dyad_list[relation==RELATION]
    
    #contenitore per i dati del gruppo
    data_rel = {}
    
    #per ogni diade nel gruppo
    for D in selected_dyads:
        
        #carica e processa il segnale del maschio
        signal_M = ph.from_pickle(f'{DATADIR}/{D}_M.pkl')
        signal_M = preprocess_signal(signal_M)
        
        #carica e processa il segnale della femmina
        signal_F = ph.from_pickle(f'{DATADIR}/{D}_F.pkl')
        signal_F = preprocess_signal(signal_F)
        
        #carca le info su quando sono stati visti i video
        trg = ph.from_pickle(f'{TRGDIR}/{D}.pkl')
        t_trg = trg.get_times()
        
        #contenitore per i dati della diade
        data_dyad = {}
        
        #per ogni video (totale 6 video)
        for v in range(6):
            V = v+1
            
            #ottieni il segmento temporale corrispondente al video
            t_video = t_trg[np.where(trg==V)[0]]
            
            if len(t_video)>0:  # se il segmento non è nullo
                
                #ottieni istanti di inizio e fine del video
                t_start = t_video[0]+10
                t_stop = t_video[-1]
                
                #prendi la porzione dal segnale del maschio e normalizza
                signal_video_M = normalize_scale(signal_M.segment_time(t_start, t_stop).get_values())
                
                if len(signal_video_M)<235*2: # se la porzione è troppo corta non fare niente
                    signal_video_M = None
                    signal_video_M_s = None
                else:
                    
                    #se la porzione è "giusta"
                    
                    #calcola il segnale surrogato del maschio
                    signal_video_M_s, _, _ = surrogate_IAAFT(signal_video_M)
                    signal_video_M_s = np.convolve(signal_video_M_s, np.ones(int(CONV_WINDOW*F_RESAMP))/int(CONV_WINDOW*F_RESAMP))
                
                
                #stesse cose per la femmina
                signal_video_F = normalize_scale(signal_F.segment_time(t_start, t_stop).get_values())
                if len(signal_video_F)<235*2:
                    signal_video_F = None
                    signal_video_F_s = None
                else:
                    signal_video_F_s, _, _  = surrogate_IAAFT(signal_video_F)
                    signal_video_F_s = np.convolve(signal_video_F_s, np.ones(int(CONV_WINDOW*F_RESAMP))/int(CONV_WINDOW*F_RESAMP))
                
                #salva i risultati del video
                data_dyad[V] = {'M': [signal_video_M, signal_video_M_s], 'F': [signal_video_F, signal_video_F_s]}
        
        #salva i risultati della diade
        data_rel[D] = data_dyad
    
    #salva i risultati del gruppo di relazione
    data[RELATION] = data_rel

#=========================================================================
    
#%% 
#qui non si fa altro che "ordinare" i dati caricati prima in modo che siano
#adatti ad usare la libreria per il calcolo della sincronia    

#GROUP SUBJECTS BY RELATION, GENDER AND VIDEO
group_1 = {}
group_2 = {}

for RELATION in ['U', 'F', 'L']:
    selected_dyads = dyad_list[relation==RELATION]
    group_1_R = {}
    group_2_R = {}
    for v in range(len(video_names)):
        group_1_v = []
        group_2_v = []
        V=v+1
        
        for dyad in selected_dyads:
            group_1_v.append([data[RELATION][dyad][V]['M'][0], data[RELATION][dyad][V]['M'][1]])
            group_2_v.append([data[RELATION][dyad][V]['F'][0], data[RELATION][dyad][V]['F'][1]])
        group_1_R[V] = group_1_v
        group_2_R[V] = group_2_v
    group_1[RELATION] = group_1_R
    group_2[RELATION] = group_2_R
    
#%% CALCOLO DELLA SINCRONIA
# piu' o meno la struttura e' simile a quella per il caricamento dei dati ...
    
#per ogni gruppo di relazione
for i_r, RELATION in enumerate(['U', 'F', 'L']):
    
    #ottieni le diadi appartenenti al gruppo 
    selected_dyads = dyad_list[relation==RELATION]
    
    #ottieni i dati del gruppo di relazione
    group_1_R = group_1[RELATION]
    group_2_R = group_2[RELATION]
    
    #prepara i contenitori della sincronia di copresenza, stimolo e surrogata
    copresence = []
    stimulus = []
    surrogate = []
    
    #per ogni video
    for v in range(len(video_names)):
        V=v+1
        
        #carica i dati del video
        group_1_v = group_1_R[V]
        group_2_v = group_2_R[V]
        
        #calcola le tre sincronie
        #nota che il comando usa il parametro LAG
        copresence_v, stimulus_v, surrogate_v = compute_distances_golland(group_1_v, group_2_v, 'all', DISTANCE, {'lag':LAG, 'normalize': NORMALIZE})
        
        #salva le tre sincronie nei contenitori
        copresence.append(copresence_v)
        stimulus.append(stimulus_v)
        surrogate.append(surrogate_v)
        
        #passa a video succesivo

    #sistema i risulati in tabelle umanamente comprensibili
    copresence_pd = pd.DataFrame(np.array(copresence), columns=selected_dyads, index = video_names)
    stimulus_pd = pd.DataFrame(np.array(stimulus), index = video_names)
    surrogate_pd = pd.DataFrame(np.array(surrogate), index = video_names)
    
    #salva le tabelle
    copresence_pd.to_csv(f'{OUTDIR}/copresence_{RELATION}.csv')
    stimulus_pd.to_csv(f'{OUTDIR}/stimulus_{RELATION}.csv')
    surrogate_pd.to_csv(f'{OUTDIR}/surrogate_{RELATION}.csv')
    
    #passa ad altro gruppo di relazione

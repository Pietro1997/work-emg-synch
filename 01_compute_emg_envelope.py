from config import BASEPATH

#%%
#importo librerie necessarie per l'esecuzione del programma
import numpy as np
import os
import gc
import pyphysio as ph

#%% imposto directories dei dati e di output
EMGDIR = f'{BASEPATH}/data/emg'
OUTDIR = f'{BASEPATH}/data/envelope'

#imposto opzioni di processing
wlen = 0.1 #lunghezza della finestra per calcolare l'envelope
wstep = 0.01 #di quanto spostarsi per avere la finestra successiva

        
#%% ricavo lista di soggetti (un file = un soggetto)
subjects = os.listdir(EMGDIR)

#%% PROCESSING

for SUB in subjects: #processo tutti i soggetti, uno alla volta
    #PER ogni Soggetto nella lista dei soggetti, 
    #alla variabile SUB assegna il nome del soggetto (= nome del file)
    #e svolgi quello che c'e' dopo

    try: # prova ad eseguire quanto segue, se qualcosa va storto, esegui quello che trovi in except (alla fine)
        
        #carica il segnale dal file del soggetto
        emg = ph.from_pickle(f'{EMGDIR}/{SUB}') 
        
        #ottieni la frequenza di campionamento del segnale
        fsamp = emg.get_sampling_freq() 
        
        #imposta inizio e fine della prima finestra
        i_st = 0
        i_sp = int(wlen*fsamp)
        
        #calcola di quanti campioni spostarsi, 
        #in base alla lunghezza della finestra e della frequenza di campionamento
        i_steps = int(wstep*fsamp)
        
        # INIZIO DEL CALCOLO DELL'ENVELOPE
        
        #crea contenitore per mantenere i valori di envelope calcolati
        envelope = [] 
        
        #esegui i passi successivi finche' la finestra e' tutta all'interno del segnale <<DIFFICILE
        while i_sp < len(emg):
            
            #calcola envelope, metodo RMS: Root-Mean-Squares
            valore_envelope = np.sqrt(np.mean(emg[i_st:i_sp]**2))
            
            #salva il valore calcolato nel contenitore
            envelope.append(valore_envelope)
            
            #aggiorna i campioni di inizio e fine della finestra successiva <<DIFFICILE
            i_st += i_steps
            i_sp += i_steps
        
        #trasforma il contenitore in un segnale, 
        #aggiungendo le info sulla frequenza di campionamento (che dipende da wstep e quindi da i_step) <<DIFFICILE
        envelope = ph.EvenlySignal(envelope, fsamp/i_steps, start_time = wlen/2)
        
        # ricampiona il segnale envelope a 10 Hz
        envelope = envelope.resample(10)
        
        #salva il segnale envelope nella cartella di output
        envelope.to_pickle(f'{OUTDIR}/{SUB}')
        
        #libera la memoria RAM
        del emg, envelope
        gc.collect()
        
    except: #istruzioni che vengono eseguite se qualcosa va storto nei passaggi precedenti
        
        #scrivi il nome del soggetto che ha avuto problemi
        print(SUB)

from config import BASEPATH

#importa librerie
import pandas as pd
import scipy.stats as stat
import numpy as np
import pingouin as pg

#rimuovi Not A Number
def noNan(x):
    return(x[~np.isnan(x)])
    

#%% Impostazioni
#cartella dei risultati da analizzare
DIST = 'cc_EMG_1'

#numero di test da usare per correzione di multiple hypotheses
N_TESTS = 1

#%% Altre impostazioni, non cambiare
SEED = 1234
np.random.seed(SEED)

video_names = ['HS', 'TIT', 'WD', 'RELAX', 'NH', 'PEN']
video_emotions = ['EMBRS', 'SAD', 'FEAR', 'CALMNESS', 'LOVE', 'PRIDE']

DIST_DIR = f'{BASEPATH}/data/{DIST}'

#%% 
#contenitore per tutti i risultati
result_all = []
i=0
#per ogni video
for i_v, VIDEO in enumerate(video_emotions):
    
    #per ogni gruppo di relazione
    for i_r, RELATION in enumerate(['U', 'F', 'L']):
        
        #carica i file con i valori di sincronia
        copresence = pd.read_csv(f'{DIST_DIR}/copresence_{RELATION}.csv', index_col=0)
        stimulus = pd.read_csv(f'{DIST_DIR}/stimulus_{RELATION}.csv', index_col=0)
        surrogate = pd.read_csv(f'{DIST_DIR}/surrogate_{RELATION}.csv', index_col=0)

        #rimuovi nans dai valori di sincronia di copresenza
        copresence_v = noNan(copresence.iloc[i_v,:])
        
        #calcola N, media e SD della sincronia di copresenza
        N_copr, mean_copr, sd_copr = len(copresence_v), np.mean(copresence_v), np.std(copresence_v)
        
        #idem per sincronia di stimolo
        stimulus_v = stimulus.iloc[i_v,:]
        N_stim, mean_stim, sd_stim = len(stimulus_v), np.mean(stimulus_v), np.std(stimulus_v)
        
        #idem per sincronia surrogata
        surrogate_v = surrogate.iloc[i_v,:]
        N_surr, mean_surr, sd_surr = len(surrogate_v), np.mean(surrogate_v), np.std(surrogate_v)

        #verifica differenza statistica delle mediane tra surrogata e stimolo       
        result_test_surr_stim = pg.mwu(surrogate_v, stimulus_v, tail='one-sided')
        U_surr_stim = result_test_surr_stim['U-val'][0]
        p_surr_stim = result_test_surr_stim['p-val'][0]
        
        #verifica differenza statistica delle mediane tra stimolo e copresenza
        result_test_stim_copr = pg.mwu(stimulus_v, copresence_v, tail='one-sided')
        U_stim_copr = result_test_stim_copr['U-val'][0]
        p_stim_copr = result_test_stim_copr['p-val'][0]
        
        #metto risultati in dizionario
        risultati_correnti = {'Relazione': RELATION, 'Emozione': VIDEO, 
                              'N_surr': N_surr, 'mean_surr': mean_surr, 'SD_surr': sd_surr,
                              'U_surr_stim': U_surr_stim, 'p_surr_stim': p_surr_stim,
                              'N_stim': N_stim, 'mean_stim': mean_stim, 'SD_stim': sd_stim,
                              'U_stim_copr': U_stim_copr, 'p_stim_copr': p_stim_copr,
                              'N_copr': N_copr, 'mean_copr': mean_copr, 'SD_copr': sd_copr}
        
        #trasformo il dizionario in rigadi tabella e la aggiungo al contenitore        
        result_all.append(pd.DataFrame(risultati_correnti, index = [i]))
        i+=1

#%%
#concateno tutte le righe        
result_all = pd.concat(result_all)

#%% CORREZIONE MULTIPLE HYPOTHESIS
#correggo pvalues surr_stim
p_uncorrected_surr_stim = result_all['p_surr_stim'].values
is_significant_surr_stim, p_corrected_surr_stim = pg.multicomp(p_uncorrected_surr_stim, method = 'fdr_bh')
result_all['issignif_surr_stim'] = is_significant_surr_stim
result_all['p_surr_stim-corr'] = p_corrected_surr_stim

#correggo pvalues stim_copr
p_uncorrected_stim_copr = result_all['p_stim_copr'].values
is_significant_stim_copr, p_corrected_stim_copr = pg.multicomp(p_uncorrected_stim_copr, method = 'fdr_bh')
result_all['issignif_stim_copr'] = is_significant_stim_copr
result_all['p_stim_copr-corr'] = p_corrected_stim_copr

result_all.to_csv(f'{BASEPATH}/results_{DIST}.csv')
